import { Link } from "react-router-dom";

export function Podcast(props) {

  return (
    <div className="column is-3-desktop is-6-tablet is-clickable">
        <Link to={`/details/${props.podcast.podcastId}`}>
            <div className="card has-text-black">
                <div className="card-image">
                    <figure className="image is-square">
                        <img alt={props.podcast.name} src={props.podcast.artworkUrl}/>
                    </figure>
                </div>
                <div className="card-content">
                    <div className="content has-text-centered mb-1">
                        <p className="title is-6">
                            { props.podcast.name }
                        </p>
                        <div>
                            <p className="text is-6">
                                { props.podcast.artist }
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </Link>
    </div>
  );
}