import React, { useState } from "react";
import { useEffect, useContext } from "react";
import { UnContexte } from "./App";
import { serveur } from "./constantes";
import {Link} from "react-router-dom";
import {useNavigate} from "react-router-dom";
import "./HomePage.css";
import { Verification, VerificationEmail, VerificationPass } from "./Verfication";

function Profile() {  
    const [estaffiche, setestaffiche] = useState(false);
    const navigate = useNavigate();
    const lecontext = useContext(UnContexte);
    
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [ConfPassword, setCongPassword] = useState("");

    function handleChangeEmail(event) { setEmail(event.target.value); }
    function handleChangePassword(event) { setPassword(event.target.value); }
    function handleChangeConfPassword(event) { setCongPassword(event.target.value); }

    useEffect(() => {
        async function getUser() {
            try {
                const response = await fetch(`${serveur}/user`, {
                    method: "GET",
                    headers: { authorization: `Bearer ${lecontext.token}` },
                });
                const data = await response.json();
                if (response.ok) {
                    setEmail(data.email);
                } 
                else {
                    alert(data.message);
                }
            } catch (err) {
                console.error("une erreur s'est produite");
            }
        }
        getUser();
    }, [lecontext.token]);



    async function deleteUser() {
        try {
            const response = await fetch(`${serveur}/user`, {
                method: "DELETE",
                headers: {
                    authorization: `Bearer ${lecontext.token}`,
                    "Content-Type": "application/json"
                },
            });
            if (response.ok) {
                lecontext.setToken("");
                localStorage.removeItem("token");
                navigate("/");
            } else {
                const data = response.json();
                alert(data.message);
            }
        } catch (err) {
            console.error("une erreur s'est produite");
        }
    }


    async function updateUser() {
        try {
            const body = { email: email, password: password };
            const response = await fetch(`${serveur}/user`, {
                method: "POST",
                headers: { authorization: `Bearer ${lecontext.token}`, "Content-Type": "application/json" },
                body: JSON.stringify(body),
            });
            response.ok ? alert("profile mis à jour") : alert("profile pas mis à jour");
        } catch (err) {
            console.error("une erreur s'est produite");
        }
    }
                                    
    return (
        <div className="section" role="form">
            <h1 className="title is-1 has-text-centered">Profile</h1>
            <div className="field">
                <label className="label" htmlFor="email">Email</label>
                <div className="control has-icons-left">
                    <input id="email" autoComplete="email" value={email}
                            className="input" placeholder="e1234567"
                            aria-required="true"  onChange={handleChangeEmail}/>
                    <span className="icon is-small is-left">
                        <i className="fa fa-envelope"></i>
                    </span>
                </div>
                <VerificationEmail email={email} />
            </div>
            <div className="field">
                <label className="label" htmlFor="password">Mot de passe</label>
                <div className="control has-icons-left">
                    <input id="password" autoComplete="password"
                            className="input" placeholder="*******"
                            type="password" aria-required="true"
                            onChange={handleChangePassword} />
                    <span className="icon is-small is-left">
                        <i className="fa fa-lock"></i>
                    </span>
                </div>
                <VerificationPass password={password} />
            </div>
            <div className="field">
                <label className="label" htmlFor="Confpassword">Confirmer ot de passe</label>
                <div className="control has-icons-left">
                    <input id="Confpassword" autoComplete="Confpassword"
                            className="input" placeholder="*******"
                            type="password" aria-required="true"
                            onChange={handleChangeConfPassword}/>
                    <span className="icon is-small is-left">
                        <i className="fa fa-lock"></i>
                    </span>
                    <Verification confPassword={ConfPassword} password={password} />
                </div>
            </div>
            <div className="field">
                <div className="control">
                    <button className={(email.includes("@") && password === ConfPassword && /[a-zA-Z!@[\]#$%&*()].{8,}/.test(password)) ? "button is-success" : "button is-success disable"} onClick={updateUser}> UPDATE PROFILE </button>&nbsp;&nbsp;
                    <button className="button is-danger" data-target="modal-ter" aria-haspopup="true" onClick={() => { setestaffiche(true); }}> DELETE PROFILE </button>&nbsp;&nbsp;
                    {
                        <div id="modal-ter" className={estaffiche ? "modal is-active" : "modal"}>
                            <div className="modal-background"></div>
                            <div className="modal-card">
                                <header className="modal-card-head">
                                    <p className="modal-card-title">ATTENTION!!</p>
                                    <button className="delete" aria-label="close" onClick={() => { setestaffiche(false); }}></button>
                                </header>
                                <section className="modal-card-body"> Voulez-vous supprimer votre compte?? </section>
                                <footer className="modal-card-foot">
                                    <button className="button is-danger" onClick={deleteUser}>DELETE</button>
                                    <button className="button" onClick={() => { setestaffiche(false); }}>Cancel</button>
                                </footer>
                            </div>
                        </div>
                    }
                    <Link className="button is-light" to="/"> Annuler </Link>
                </div>
            </div>
        </div>
    );
}

export {Profile};
