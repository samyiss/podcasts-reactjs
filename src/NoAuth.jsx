import "./HomePage.css";

function NoAuth() {
    return (
        <div className="section">
            <h1 className="title is-1 has-text-centered">401 Not Authenticated</h1>
        </div>
    );
}

export {NoAuth};
