import { useContext } from "react";
import {Link} from "react-router-dom";
import { UnContexte } from "./App";
import {useNavigate} from "react-router-dom";
import { useState } from "react";

export function Menu() {

    const lecontext = useContext(UnContexte);
    const navigate = useNavigate();
    const [show, setShow] = useState(false);

    function logOut() {
        navigate("/");
        lecontext.setToken("");
        localStorage.removeItem("token");
    }

    return (
        <nav className="navbar">
            <div className="navbar-brand" onClick={ () => { show ? setShow(false) : setShow(true); } }>
                <button className="navbar-burger" data-target="navMenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
            <div className={ show ? "navbar-menu is-active" : "navbar-menu"} id="navManu" role="navigation" aria-label="main navigation">
                <div className="navbar-start">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Link className="navbar-item" to="/">Home</Link>
                </div>
                <div className="navbar-end">
                    {
                        lecontext.token !== "" ?
                            <div className="buttons">
                                <Link className="button is-light" to="/subscription">Subscriptions</Link>
                                <Link className="button is-light" to="/profile">Profile</Link>
                                <Link className="button is-light is-danger" to="/" onClick={logOut}>Logout</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        :
                            <div className='buttons'>
                                <Link className="button is-light" to="/signup">Sign Up</Link>&nbsp;
                                <Link className="button is-light" to="/login">Log in</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                    }     
                </div>
            </div>
        </nav>
    );
}
