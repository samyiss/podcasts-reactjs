export function Episode(props) {

    return (
        <div>
            <ul>
                <li><b>Title: </b><label htmlFor="details">­{props.episode.title}</label></li>
                <li><b>Content: </b><label htmlFor="details">{props.episode.content}</label></li>
                <li><b>Date: </b><label htmlFor="details">{props.episode.isoDate}</label></li>
                <li>
                    <audio controls preload="metadata" src={props.episode.audioUrl} alt={props.episode.title} >
                        <source type="audio/ogg" />
                            Your browser does not support the audio element.
                    </audio>
                </li>
            </ul>
            <br/>
            <br/>
        </div>
    );
}