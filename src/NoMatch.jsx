import "./HomePage.css";

function NoMatch() {
    return (
        <div className="section">
            <h1 className="title is-1 has-text-centered">404 Page Not Found</h1>
        </div>
    );
}

export {NoMatch};
