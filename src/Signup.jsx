import React, { useState } from "react";
import { serveur } from "./constantes";
import {useNavigate} from "react-router-dom";
import "./HomePage.css";
import { Verification, VerificationEmail, VerificationPass } from "./Verfication";

function Signup() {  

    const navigate = useNavigate();
    
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [ConfPassword, setCongPassword] = useState("");

    function handleChangeEmail(event) { setEmail(event.target.value); }
    function handleChangePassword(event) { setPassword(event.target.value); }
    function handleChangeConfPassword(event) { setCongPassword(event.target.value); }

    async function inscription() {
        try {
            const body = {
                email: email,
                password: password,
            };
            const response = await fetch(`${serveur}/auth/register`, {
                headers: { "Content-Type": "application/json" },
                method: "POST",
                body: JSON.stringify(body),
            });
            if (response.ok) {
                navigate("/login");
            } else {
                const data = await response.json();
                alert(data.message);
            }
        } catch (err) {
            console.error("une erreur s'est produite");
        }
    }

    
    return (
        <div className="section" role="form">
            <h1 className="title is-1 has-text-centered">Sign Up</h1>
            <div className="field">
                <label className="label" htmlFor="email">Email</label>
                <div className="control has-icons-left">
                    <input id="email" autoComplete="email"
                            className="input" placeholder="e1234567"
                            aria-required="true" onChange={handleChangeEmail}/>
                    <span className="icon is-small is-left">
                        <i className="fa fa-envelope"></i>
                    </span>
                </div>
                <VerificationEmail email={email} />
            </div>
            <div className="field">
                <label className="label" htmlFor="password">Mot de passe</label>
                <div className="control has-icons-left">
                    <input id="password" autoComplete="password"
                            className="input" placeholder="*******"
                            type="password"
                            aria-describedby="passwdValid"
                            aria-required="true" onChange={handleChangePassword}/>
                    <span className="icon is-small is-left">
                        <i className="fa fa-lock"></i>
                    </span>
                </div>
                < VerificationPass password={password} />
            </div>
            <div className="field">
                <label className="label" htmlFor="Confpassword">Confirmer ot de passe</label>
                <div className="control has-icons-left">
                    <input id="Confpassword" autoComplete="Confpassword"
                            className="input" placeholder="*******"
                            type="password" aria-required="true"
                            onChange={handleChangeConfPassword}/>
                    <span className="icon is-small is-left">
                        <i className="fa fa-lock"></i>
                    </span>
                </div>
                <Verification  confPassword={ConfPassword} password={password} />
            </div>
            <div className="field">
                <div className="control">
                    <button className={(email.includes("@") && password === ConfPassword && /[a-zA-Z!@[\]#$%&*()].{8,}/.test(password)) ? "button is-success" : "button is-success disable"} onClick={inscription}> Sign Up </button>
                    <button className="button is-danger"> Annuler </button>
                </div>
            </div>
        </div>
    );
}

export {Signup};
