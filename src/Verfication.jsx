export function VerificationPass(props) {

    return (
        <div className="subtitle is-size-7">
            {    
                (props.password !== "" && /[a-z\s]/.test(props.password)) ? 
                    <span key="2" className="has-text-primary"> ✓ Contient au moins une lettre minuscule (a-z)<br/></span>
                : 
                    <span key="2" className="has-text-danger"> ✗ Contient au moins une lettre minuscule (a-z)<br/></span>
            }
            {
                (props.password !== "" && /[A-Z\s]/.test(props.password)) ? 
                    <span key="3" className="has-text-primary"> ✓ Contient au moins une lettre majuscule (A-Z)<br/></span>
                : 
                    <span key="3" className="has-text-danger"> ✗ Contient au moins une lettre majuscule (A-Z)<br/></span>
            }      
            { 
                (props.password !== "" && /.{8,}/.test(props.password)) ? 
                    <span key="4" className="has-text-primary"> ✓ Contient au moins 8 caractéres<br/></span>
                : 
                    <span  key="4" className="has-text-danger"> ✗ Contient au moins 8 caractéres<br/></span>
            }
            {       
                (props.password !== "" && /[!@[\]#$%&*()\s]/.test(props.password)) ? 
                    <span key="5" className="has-text-primary"> ✓ Contient au moins un symbole parmi les suivants: !@#$%&*()[]<br/></span>
                : 
                    <span  key="5" className="has-text-danger"> ✗ Contient au moins un symbole parmi les suivants: !@#$%&*()[]<br/></span> 
            }
        </div>
    );
}

export function VerificationEmail(props) {
    return(
        <div className="subtitle is-size-7">
            {
                (props.email !== "" && props.email.includes("@")) ?
                    <p key="1" className="has-text-primary"> ✓ Contient le caractére @ </p>
                : 
                    <p key="1" className="has-text-danger"> ✗ Contient le symbole @ </p>
            }
        </div>
    );
}

export function Verification(props) {
    return(
        <div className="subtitle is-size-7">
            {
                (props.confPassword === "") ? 
                    ""
                :
                    (props.confPassword !== props.password || props.password === "" || props.confPassword === "") ? 
                        <span className="has-text-danger"> ✗ La confirmation et le mot de passe doivent être identique </span>
                    : 
                        <span className="has-text-primary"> ✓ La confirmation et le mot de passe doivent être identique </span>
            }
        </div>
    );
}