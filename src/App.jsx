import {BrowserRouter, Route, Routes} from "react-router-dom";
import React, {useState} from "react";
import {HomePage} from "./HomePage";
import {Login} from "./Login";
import {Menu} from "./Menu";
import {Footer} from "./Footer";
import {Signup} from "./Signup";
import {Profile} from "./Profile";
import {Details} from "./Details";
import {Subscriptions} from "./Subscriptions";
import { NoAuth } from "./NoAuth";
import { NoMatch } from "./NoMatch";

export const UnContexte = React.createContext();

export default function App() {
    const [token, setToken] = useState(localStorage.getItem("token")? localStorage.getItem("token") : "");
    const objetsEtMethodesDuContexte = { token, setToken };

    return (
        <UnContexte.Provider value={objetsEtMethodesDuContexte}>
            <BrowserRouter>
                <Menu/>
                <Routes>
                    <Route path="/" element={<HomePage/>}/>
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/signup" element={<Signup/>}/>
                    <Route path="/profile" element={token === "" ? <NoAuth/> : <Profile/>}/>
                    <Route path="/details/:idPodcast" element={<Details/>}/>
                    <Route path="/subscription" element={token === "" ? <NoAuth/> : <Subscriptions/>}/>
                    <Route path="*" element={<NoMatch />} />
                </Routes>
                <Footer/>
            </BrowserRouter>
        </UnContexte.Provider>
    );
}
