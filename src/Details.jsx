import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import { serveur } from "./constantes";
import { useContext } from "react";
import { UnContexte } from "./App";
import { Episode } from "./Episode";

function Details() {
    const params = useParams();

    const [details, setDetails] = useState(null);
    const [subscription, setSubscrip] = useState();

    const lecontext = useContext(UnContexte);

    useEffect(() => {
        async function getDetails() {
            const response = await fetch(`${serveur}/podcast?podcastId=${params.idPodcast}`);
            const data = await response.json();
            setDetails(data);
        }
        getDetails();
    }, [params.idPodcast]);


    async function ajoutSub() {
        const bearer = "Bearer " + lecontext.token;

        await fetch(`${serveur}/subscription?podcastId=${params.idPodcast}`, {
            method: "POST",
            headers: { authorization: bearer },
            body: { isSubscribed: true },
        });

        setSubscrip(true);
    }

    async function SupprimerSub() {
        const bearer = "Bearer " + lecontext.token;
        await fetch(`${serveur}/subscription?podcastId=${params.idPodcast}`, {
            method: "DELETE",
            headers: { authorization: bearer },
            body: { isSubscribed: false },
        });
        setSubscrip(false);
    }


    useEffect(() => {
        async function verifSub() {
            const rep = await fetch(`${serveur}/subscription?podcastId=${params.idPodcast}`, {
                method: "GET",
                headers: { authorization: `Bearer ${lecontext.token}` },
            });
            const res = await rep.json();
            setSubscrip(res.isSubscribed);
        }

        if(lecontext.token !== "") {
            verifSub();
        }
    }, [params.idPodcast, lecontext.token]);
    
    return (
        details !== null &&
        <div className="section" >
            <ul className="has-text-centered">
                <div>
                    {
                        lecontext.token === "" ?
                            <div></div>
                        :                                
                            subscription === true ?
                                <button className="button is-danger" onClick={SupprimerSub}> Unsubscribe </button>
                            :
                                <button className="button is-success" onClick={ajoutSub}> Subscribe </button>
                    }
                </div>
                <br/>
            </ul>
            <ul className="has-text-centered">
                <li><img alt={details.name} src={details.artworkUrl}/></li>
            </ul>
            <br/>
            <ul>
                <li><b>Name: </b><label htmlFor="details">{details.name}</label></li>
                <li><b>Artists: </b><label htmlFor="details">{details.artist}</label></li>
                <li><b>Genres: </b><label htmlFor="details">{details.genres.map((e) => e).join(", ")}</label></li>
                <li><b>Description: </b><label htmlFor="details">{details.description}</label></li>
                <li><b>Episodes: </b><label htmlFor="details">{details.height}</label></li><br/>
                { details.episodes.map((episode) => <Episode episode={episode} key={episode.episodeId}/>) }
            </ul>
        </div>
    );
}

export {Details};
