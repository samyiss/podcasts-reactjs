export function Footer() {

  return (
    <footer className="footer">
        <div className="content has-text-centered">
            <p>© e2072931</p>
        </div>
    </footer>
  );
}