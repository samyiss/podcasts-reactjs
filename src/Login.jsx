import React, { useState, useContext } from "react";
import { serveur } from "./constantes";
import {useNavigate} from "react-router-dom";
import { UnContexte } from "./App";

function Login() {    
    const navigate = useNavigate();
    const lecontext = useContext(UnContexte);

    const [email, setEmail] = useState("");
    function handleChangeEmail(event) { setEmail(event.target.value); }

    const [password, setPassword] = useState("");
    function handleChangePassword(event) { setPassword(event.target.value); }

    async function login() {
        const body = { email: email, password: password };
        fetch(`${serveur}/auth/token`, {
            headers: { "Content-Type": "application/json" },
            method: "POST",
            body: JSON.stringify(body),
        }).then(async (res) => {
            const data = await res.json();
            if (res.ok) {
                localStorage.setItem("token", data.token);
                lecontext.setToken(data.token);
                navigate("/subscription");
            }
            else {
                alert(data.message);
            }
        }
        ).catch((error) => {
            console.error(error);
        });
    }

    return (
        <div className="section">
            <div className="content">
                <div className="field">
                    <label htmlFor="email" className="label">Email</label>
                    <div className="control has-icons-left">
                        <input type="email" placeholder="e1234567@site.com" className="input"
                            autoComplete="email" required onChange={handleChangeEmail}/>
                        <span className="icon is-small is-left"><i className="fa fa-envelope"></i></span>
                    </div>
                </div>
                <div className="field">
                    <label htmlFor="password" className="label">Password</label>
                    <div className="control has-icons-left">
                        <input type="password"  placeholder="*******" className="input" autoComplete="password"
                            required onChange={handleChangePassword}/>
                        <span className="icon is-small is-left"><i className="fa fa-lock"></i></span>
                    </div>
                </div>
                <div className="field">
                    <div className="control">
                        <button className="button is-success" onClick={login}>Connexion</button>
                        <button className="button is-danger">Annuler</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export {Login};
