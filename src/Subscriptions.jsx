import React, { useState, useEffect } from "react";
import { serveur } from "./constantes";
import {useContext} from "react";
import { UnContexte } from "./App";
import { Podcast } from "./Podcast";

function Subscriptions() {
    const lecontext = useContext(UnContexte);
    const [podcasts, setpodcasts] = useState([]);
    
    useEffect(() => {
        async function getSubscriptions() {
            const bearer = "Bearer " + lecontext.token;
            const rep = await fetch(`${serveur}/subscriptions`, {
                method: "GET",
                headers: { authorization: bearer },
            });
            if (rep.ok) {
                const data = await rep.json();
                setpodcasts(data);
            }
        }
        getSubscriptions();
    });


    return (
        podcasts !== null &&
        <div className="section">
            <h1 className="title is-1 has-text-centered">Subscriptions</h1>
            <div className="row columns is-multiline">
                {podcasts.map( (podcast) => <Podcast podcast={podcast} key={podcast.podcastId}/> )}
            </div>
        </div>
    );
}

export {Subscriptions};
