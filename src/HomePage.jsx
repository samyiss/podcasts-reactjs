import React, { useState, useEffect } from "react";
import {Podcast} from "./Podcast";
import { serveur } from "./constantes";
import "./HomePage.css";

function HomePage() {

    const [podcast, setpodcast] = useState([]);
    const [podcasts, setpodcasts] = useState([]);
    const [podcastPages, setpodcastPages] = useState([]);
    const [btnPage, setBtnPage] = useState([]);

    const [pageCourante, setPageCourante] = useState(1);

    const [taillePage, setTaillePage] = useState(parseInt(localStorage.getItem("taillePage"))? parseInt(localStorage.getItem("taillePage")) : 8);
    function handleChangePage(event) {
        setPageCourante(1);
        localStorage.setItem("taillePage", parseInt(event.target.value));
        setTaillePage(parseInt(event.target.value));
    }

    const [nbPages, setNbPages] = useState(0);

    const [nomPodcast, setNomPodcast] = useState("");
    function handleChangeNom(event) { setNomPodcast(event.target.value); }   
    
    function changerPage(nbPage) { setPageCourante(nbPage); }
    
    function pagePrecedente() {
        let pCour = pageCourante;
        if (pageCourante >= 2) setPageCourante(pCour -= 1);
    }
    function pageSuivante() {
        let pCour = pageCourante;
        if (pageCourante < nbPages) setPageCourante(pCour += 1);
    }
    useEffect( () => {
        async function getpodcast() {
            const rep = await fetch(`${serveur}/podcasts/top`);
            if (rep.ok) {
                const data = await rep.json();
                setpodcast(data);
            }
        }
        getpodcast();
    }, []);
        
    useEffect(() => {           
        if (nomPodcast !== "") {
            setPageCourante(1);
            setpodcasts(podcast.filter(
                (t) => t.name.toLowerCase().includes(nomPodcast.toLowerCase()),
            ));
        } else {
            setpodcasts(podcast);
        }
    }, [nomPodcast, podcast]);

    useEffect(() => {
        const nbTvShow = (pageCourante - 1) * taillePage;
        const podcastPage = podcasts.slice(nbTvShow, nbTvShow + taillePage);
        setpodcastPages(podcastPage);
    }, [pageCourante, podcasts, taillePage]);

    useEffect( () => {

        const nvPodcats = podcasts.length;

        function nbPage() {
            let nbPag = Math.round(nvPodcats / taillePage);
            if (nvPodcats % taillePage > 0) nbPag += 1;
            setNbPages(nbPag);
            return nbPag;
        }

        const btn = [];
        for (let i = 1; i <= nbPage(); i++) {
            btn.push(<li key={i}>
                <button className={ pageCourante === i ? "pagination-link is-current" : "pagination-link"} onClick={() => { changerPage(i); }} >{i}</button>
            </li>);
        }
        setBtnPage(btn);
    }, [pageCourante, podcasts.length, taillePage]);


  return (
    <div className="section">
        <div className="field is-horizontal">
            <div className="field-label is-normal">
                <label htmlFor="titre" className="label">Titre</label>
            </div>
            <div className="field-body">
                <div className="field">
                    <p className="control is-expanded">
                        <input
                            id="titre" className="input" type="text"
                            placeholder="Titre du podcast" onChange={handleChangeNom}/>
                    </p>
                </div>
            </div>
        </div>
        <br></br>
        <div>
          <div className="row columns is-multiline">
            { podcastPages.map( (podcast) => <Podcast podcast={podcast} key={podcast.podcastId}/> ) }
          </div>
        </div>
        <p id="pag">&nbsp;</p>
        <nav className="pagination" role="navigation" aria-label="pagination" >
            <button className={pageCourante === 1 ? "pagination-previous disable" : "pagination-previous"} onClick={pagePrecedente}>&lt;</button>
            <button className={pageCourante === nbPages ? "pagination-next disable" : "pagination-next"} onClick={pageSuivante}>&gt;</button>
            <ul className="pagination-list" >
                { btnPage }
            </ul>
        </nav>
        <div className="is-vertival is-mobile is-pulled-right">
            <label className="label">Nombre de podcasts par page:  &nbsp;&nbsp;
                <div className="select">
                    <select value={taillePage} onChange={handleChangePage}>
                        <option>8</option>
                        <option>12</option>
                        <option>24</option>
                    </select>
                </div>
            </label>
        </div>
    </div>
  );
}

export {HomePage};
